import Add from "./modules/add.js"
import List from "./modules/list.js";
import Storage from "./modules/storage.js";
import Search from "./modules/search.js";

export default class App{
    constructor(){
        //Inicializar propiedades
        this.add = new Add();
        this.list = new List();
        this.storage = new Storage();
    }

    load(){
        // Añadir libros
        this.add.libroSave();

        //Conseguir array objetos localStorage
        const libros = this.storage.getData();

        // listar libros
        this.list.show(libros);

        //buscar libros
        Search()

        console.log('La aplicación de libros a sido inicializada')

    }
}