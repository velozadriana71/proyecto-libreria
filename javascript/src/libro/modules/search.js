import List from "./list.js";
import Storage from "./storage.js";

export default function () {
  // Crear los objetos
  const storage = new Storage();
  const list = new List();

  // Conseguir los datos de los libros
  let content = document.querySelector("#content");
  let search_btn = document.querySelector("#search");

  // Aplicar evento al formulario de búsqueda
  search_btn.onclick = (e) => {
    e.preventDefault();

    //Conseguir el texto introducido en el campo de búsqueda
    let wanted = document.querySelector("#search_field").value;

    // Conseguir datos de libros actualizados
    let libros_stored = storage.getData();

    // Aplicar filtro para encontrar libros
    const new_libros = libros_stored.filter((libro) => {
      return libro.title.toLowerCase().includes(wanted.toLowerCase());
    });

    // Mostrar el listado de coincidencias
    if (new_libros.length <= 0) {
      content.innerHTML = "<div><h2>No hay coincidencias.</h2></div>";
    } else {
      list.show(new_libros);
    }

    return false;
  };
}
