import deleteOfList from "./delete.js";
import edit from "./edit.js";

export default class List {
  constructor() {
    //Seleccionar elementos del DOM al usar
    this.content = document.querySelector("#content");
  }

  libroTemplate(libro) {
    return `<article class='libro-item' id='libro-${libro.id}'>
        <h3 class='title'>${libro.title}</h3>
        <p class='description'>${libro.description}</p>

        <button class='edit' data-id='${libro.id}'>Editar</button>
        <button class='delete' data-id='${libro.id}'>Borrar</button>
        </article>`;
  }

  show(libros) {
    //Vaciar dom del contenedor principal de libros
    this.content.innerHTML = "";

    //Recorrer y mostrar todos los objetos/libros del localStorage
    libros.forEach((libro) => {
      this.content.innerHTML += this.libroTemplate(libro);
    });

    //Funcionalidad botones de borrado
    deleteOfList();

    //Funcionalidad botones de edición
    edit();
  }
}
