import Storage from "./storage.js";
import List from "./list.js";

export default function () {
  // Crear objetos
  const storage = new Storage();
  const list = new List();

  // Conseguir datos de libros
  let libros_stored = storage.getData();
  let libros_viewed = document.querySelectorAll(".libro-item");

  // Recorrer libros mostrados
  libros_viewed.forEach((libro) => {
    // Seleccionar el botón de editar
    let edit_btn = libro.querySelector(".edit");

    // Asignar un evento click
    edit_btn.onclick = function () {
      //Conseguir id del libro a editar
      const libro_id = parseInt(this.getAttribute("data-id"));

      // Quitar botones
      edit_btn.remove();
      libro.querySelector(".delete").remove();

      // Añadir un trozo de html debajo de los botones
      let libro_edit_html = `
            <div class='edit_form'>
            <h3 class='title'>Editar libro</h3>
            <form>
            <input type="text" class="edited_title" value="${
              libro.querySelector(".title").innerHTML
            }"/>
            <textarea class='edited_description'>${
              libro.querySelector(".description").innerHTML
            }</textarea>
           <input type="submit" class="update" value="actualizar" />
            </form>
            </div>
            `;

      libro.innerHTML += libro_edit_html;

      // Seleccionar el botón de actualizar
      let update_btn = libro.querySelector(".update");

      // Aplicar evento click
      update_btn.onclick = function (e) {
        e.preventDefault();
        // Buscar indice del libro  a actualizar
        let index = libros_stored.findIndex((libro) => libro.id === libro_id);

        // Guardar objeto dentro de ese indice
        libros_stored[index] = {
          id: libro_id,
          title: libro.querySelector(".edited_title").value,
          description: libro.querySelector(".edited_description").value,
        };

        // Actualizar localStorage
        storage.save(libros_stored);

        // Volver a mostrar el listado
        list.show(libros_stored);

        return false;
      };
    };
  });
}
