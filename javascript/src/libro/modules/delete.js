import List from "./list.js";
import Storage from "./storage.js";

export default function () {
  //Crear objetos
  const storage = new Storage();
  const list = new List();

  //Datos de los libros disponibles
  let libros_stored = storage.getData();
  let libros_viewed = document.querySelectorAll("#content .libro-item");

  //Recorrer libros mostrados
  libros_viewed.forEach((libro) => {
    //Capturar el botón
    let delete_btn = libro.querySelector(".delete");

    //Aplicar un evento click
    delete_btn.onclick = function () {
      //Conseguir el id del libro que quiero borrar
      const libro_id = this.getAttribute("data-id");

      //Filtrar el array para que elimine la que no quiero
      const new_libros_stored = libros_stored.filter(
        (libro) => libro.id !== parseInt(libro_id)
      );

      //Actualizar datos en localStorage
      storage.save(new_libros_stored);

      //Volver a mostrar listado actualizado
      list.show(new_libros_stored);
    };
  });
}
