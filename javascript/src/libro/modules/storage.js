export default class Storage {
  constructor() {
    this.id = 1;
  }

  getData() {
    let libros = JSON.parse(localStorage.getItem("libro"));
    if (!libros || libros.length < 1) {
      libros = [
        {
          id: 0,
          title: "Harry Potter",
          description: "Las reliquias de la muerte parte 1.",
        },
      ];
      this.id = 1;
    } else {
      this.id = libros[libros.length - 1].id + 1;
    }
    return libros;
  }

  getLastId(){
    return this.id;
  }

  save(data) {
    localStorage.setItem("libro", JSON.stringify(data));
  }
}
