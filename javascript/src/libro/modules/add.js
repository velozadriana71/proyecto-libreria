import Storage from "./storage.js";
import List from "./list.js";

export default class Add {
  constructor() {
    //Crear objetos
    this.storage = new Storage();
    this.list = new List();

    // Conseguir elementos del DOM a utilizar
    this.title_field = document.querySelector("#title");
    this.description_field = document.querySelector("#description");
    this.save_btn = document.querySelector("#add");
  }

  libroSave() {
    this.save_btn.onclick = (e) => {
      e.preventDefault();

      //Conseguir datos actualizados
      let libros = this.storage.getData();
      let lastId = this.storage.getLastId();

      //Datos a guardar
      let title = this.title_field.value;
      let description = this.description_field.value;

      //Validación
      if (title != "" || description != "") {
        //Crear objeto a guardar
        let libro = {
          id: lastId++,
          title,
          description,
        };

        //Añadir al array de objetos
        libros.push(libro);

        //Guardar en el localStorage
        this.storage.save(libros);

        //Actualizar el listado
        // this.list.addToList(libro, libros);
        this.list.show(libros);
      } else {
        alert("Introduce información en todos los campos del formulario");
      }

      return false;
    };
  }
}
